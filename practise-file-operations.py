with open("data/example.txt") as f:
    for i, line in enumerate(f):
        if i == 0: # skips line indexed 0 so skipping the first line
            continue
        clean_line=line.rstrip()
        if clean_line=="":
            continue
        print(clean_line)

# print(" ")
# print(" *** NEW LINE *** ")
# print(" ")

with open("data/sample.txt", "w+") as f:
    for i in range(10):
        f.write(f"This is line number {i}\n")

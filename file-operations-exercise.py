words_dict = {}

with open("data/LoremIpsum.txt") as f:
    for i, line in enumerate(f):
        if i == -1:
            continue

        clean_line = line.rstrip()
        clean_line = clean_line.lower()

        if clean_line == "":
            continue

        for char in (".", ",", "!", "?"):
            clean_line = clean_line.replace(char, "")

        words = clean_line.split(" ")

        for word in words:
            if words_dict.get(word) is not None:
                words_dict[word] += 1
            else:
                words_dict[word] = 1
    print(words_dict)

with open("data/WordCount.txt", "w+") as f:
    for word in words_dict:
        f.write(f"Word {word} count: {words_dict[word]}.\n")

open("LoremIpsum.txt", "r")
